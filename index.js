const axios = require('axios');

const required = (varName) => {
  throw new Error(`${varName} is required.`);
};

/**
 * Authenricates a user in the provided subdomain
 *
 * @param {string} [subdomain=required('subdomain')]
 * @param {object} [{
 *     name = required('name'),
 *     email = required('email'),
 *     appName = 'external app',
 *     mobile = '',
 *   }={}]
 * @return {Promise}
 */
const authenticate = async (
  subdomain = required('subdomain'),
  {
    name = required('name'),
    email = required('email'),
    appName = 'external app',
    mobile = '',
  } = {}
) => {
  try {
    const { data } = await axios({
      method: 'post',
      url: `https://api.zervise.com/auth/user/external-auth/${subdomain}`,
      data: {
        name,
        email,
        appName,
        mobile,
      },
    });

    if (!data) throw new Error('Something went wrong. Check your subdomain.');

    return data;
  } catch (error) {
    return error.response.data || error;
  }
};

/**
 * Creates a new ticket in the provided subdomain
 *
 * @param {string} [token=required('token')]
 * @param {string} [subdomain=required('subdomain')]
 * @param {object} [{ description = required('description'), priority = 'low', tags = [] }={}]
 * @return {Promise}
 */
const createTicket = async (
  token = required('token'),
  subdomain = required('subdomain'),
  { description = required('description'), priority = 'low', tags = [] } = {}
) => {
  try {
    const { data } = await axios({
      method: 'post',
      url: `https://api.zervise.com/auth/user/external-app/${subdomain}`,
      data: {
        token,
        description,
        priority,
        tags,
      },
    });

    if (!data) throw new Error('Something went wrong. Check your subdomain.');

    return data;
  } catch (error) {
    return error.response.data || error;
  }
};

/**
 * Authenticates and creates a new ticket in the provided subdomain
 *
 * @param {string} [subdomain=required('subdomain')]
 * @param {object} [{
 *     name = required('name'),
 *     email = required('email'),
 *     appName = 'external app',
 *     mobile = '',
 *     description = required('description'),
 *     priority = 'low',
 *     tags = [],
 *   }={}]
 * @return {Promise}
 */
const authAndCreate = async (
  subdomain = required('subdomain'),
  {
    name = required('name'),
    email = required('email'),
    appName = 'external app',
    mobile = '',
    description = required('description'),
    priority = 'low',
    tags = [],
  } = {}
) => {
  try {
    const { data } = await axios({
      method: 'post',
      url: `https://api.zervise.com/auth/user/external-app/${subdomain}`,
      data: {
        name,
        email,
        appName,
        mobile,
        description,
        priority,
        tags,
      },
    });

    if (!data) throw new Error('Something went wrong. Check your subdomain.');

    return data;
  } catch (error) {
    return error.response.data || error;
  }
};

/**
 * Returns the tikcets of a specific user
 *
 * @param {string} [token=required('token')]
 * @return {Promise}
 */
const getUserTickets = async (token = required('token')) => {
  try {
    const { data } = await axios({
      method: 'get',
      url: `https://api.zervise.com/ticket/user`,
      headers: {
        'auth-token': token,
      },
    });
    if (!data) throw new Error('Something went wrong. Check auth token.');

    return data;
  } catch (error) {
    return error.response.data || error;
  }
};

/**
 * Creates a reply in the ticket as the user
 *
 * @param {string} [token=required('token')]
 * @param {string} [ticketId=required('ticketId')]
 * @param {object} [{
 *     message = required('message')
 *   }={}]
 * @return {Promise}
 */
const replyAsUser = async (
  token = required('token'),
  ticketId = required('ticketId'),
  { message = required('message') } = {}
) => {
  try {
    const { data: user } = await axios({
      method: 'get',
      url: `https://api.zervise.com/person/auth/current`,
      headers: {
        'auth-token': token,
      },
    });
    if (
      !user ||
      !user.clientId ||
      !user._id ||
      user.role !== 'user' ||
      !user.verified
    )
      throw new Error('Something went wrong. Check auth token.');

    let formData = new FormData();
    formData.append('message', message);
    formData.append('messageType', 'reply');

    // using fetch instead of axios for known issue with form data in axios with Node v18
    const response = await fetch(
      `https://api.zervise.com/ticket/attachment/${ticketId}?clientId=${user.clientId}&updatedBy=${user._id}`,
      {
        method: 'POST',
        headers: {
          'auth-token': token,
          clientId: user.clientId,
        },
        body: formData,
      }
    );
    if (!response || !response.ok)
      throw new Error('Something went wrong. Check auth token.');

    const ticket = await response.json();
    if (!ticket) throw new Error('Something went wrong. Check auth token.');

    return ticket;
  } catch (error) {
    console.log({ error });
    return error.response.data || error;
  }
};

/**
 * Returns the faq articles of your company for users
 *
 * @param {string} [subdomain=required('subdomain'),]
 * @return {Promise}
 */
const getUserFaqs = async (subdomain = required('subdomain')) => {
  try {
    const { data: company } = await axios({
      method: 'get',
      url: `https://api.zervise.com/company/subdomain/${subdomain}`,
    });
    if (!company || !company._id)
      throw new Error('Something went wrong. Check your subdomain.');

    const { data: faqs } = await axios({
      method: 'get',
      url: `https://api.zervise.com/faqs/users/${company._id}`,
    });
    if (!faqs) throw new Error('Something went wrong. Check your subdomain.');

    return faqs;
  } catch (error) {
    return error.response.data || error;
  }
};

module.exports = {
  authenticate,
  createTicket,
  authAndCreate,
  getUserTickets,
  replyAsUser,
  getUserFaqs,
};
