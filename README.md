<p align="center">
<br>
  <a href="https://zervise.com/" rel="noopener" target="_blank"><img width="150" src="https://api.zervise.com/service-desk-server/static/zervise-logo.png" alt="Zervise Logo"></a>
</p>

<h1 align="center">Zervise API for Node JS</h1>

<div align="center">

A Promise based Node.js wrapper of Zervise API for easily integrating any Node app with Zervise's customer support portal and ticketing system.

[![license](https://img.shields.io/npm/l/@zervise/zervise-api)](https://gitlab.com/npm-zervise/npmjs-zervise/-/blob/master/LICENSE)
[![npm latest package](https://img.shields.io/npm/v/@zervise/zervise-api/latest)](https://www.npmjs.com/package/@zervise/zervise-api)
[![npm downloads](https://img.shields.io/npm/dm/@zervise/zervise-api)](https://www.npmjs.com/package/@zervise/zervise-api)
[![gitlab start](https://badgen.net/gitlab/stars/npm-zervise/npmjs-zervise)](https://www.npmjs.com/package/@zervise/zervise-api)
[![minified size](https://badgen.net/bundlephobia/min/@zervise/zervise-api)](https://bundlephobia.com/package/@zervise/zervise-api)
[![install size](https://packagephobia.com/badge?p=@zervise/zervise-api)](https://packagephobia.com/result?p=@zervise/zervise-api)
[![last commit](https://badgen.net/gitlab/last-commit/npm-zervise/npmjs-zervise)](https://gitlab.com/npm-zervise/npmjs-zervise)
[![closed issues](https://badgen.net/gitlab/closed-issues/npm-zervise/npmjs-zervise)](https://gitlab.com/npm-zervise/npmjs-zervise/-/issues)
[![open issues](https://badgen.net/gitlab/open-issues/npm-zervise/npmjs-zervise)](https://gitlab.com/npm-zervise/npmjs-zervise/-/issues)

</div>

## Table of Contents

- [Zervise](#zervise)
- [Features](#features)
- [Installing](#installing)
- [Your Zervise Subdomain](#your-zervise-subdomain)
- [Examples](#examples)
- [Zervise API](#zervise-api)
- [Resources](#resources)
- [Report Issues](#issues)
- [License](#license)

## Zervise

- Add an efficient ticketing system for free, Free Plan comes with No credit card, upto 5 agents
- Unlimited Users
- Unlimited Tickets
- SLA Management
- Email Notification
- Ticket Creation by Web
- Ticket creation through Chat
- Real Time Chat with Agent
- Real Time Ticket Update

## Features

- Integrate your [Zervise](https://zervise.com) subdomain with your other app.
- Add an efficient ticketing system for your customers.
- View tickets to manage your customer complaints.
- Authenticate your users in Zervise subdomain with their email.
- Show the FAQs for your Zervise subdomain in your other apps.

## Installing

- Using npm:

```bash
$ npm install @zervise/zervise-api
```

- Using yarn:

```bash
$ yarn add @zervise/zervise-api
```

## Your Zervise Subdomain

#### Sign Up in Zervise

For using this package you need a Zervise account and a Zervise subdomain.<br>
To create a `Free Zervise Account and get your own Zervise subdomain` head over to this link 👉 [Sign Up in Zervise](https://zervise.com/).

#### Find your Zervise subdomain

Upon succesfull sign up you should receive one email with your zervise subdomain link in the registered email address.

#### Example

If your link is `https://zervisefree-qee_ro1a8jwr.zervise.com/`, then your zervise subdoamin is `zervisefree-qee_ro1a8jwr`.

## Examples

#### Import to your project

```js
import {
  authenticate,
  createTicket,
  authAndCreate,
  getUserTickets,
  replyAsUser,
  getUserFaqs,
} from 'zervise-integration';
```

or,

```js
const {
  authenticate,
  createTicket,
  authAndCreate,
  getUserTickets,
  replyAsUser,
  getUserFaqs,
} = require('zervise-integration');
```

#### Authenticate and Create Tickets

- 2 step process

```js
// Step 1: Authenticate user
authenticate('zervisefree-qee_ro1a8jwr', {
  name: 'test-user',
  email: 'test-user@mail.com',
  mobile: '+918555198553', // optional
  appName: 'my-app', // optional
})
  .then((data) => {
    console.log(data);

    // Step 2: Create a ticket with the tocken recived
    createTicket(data.token, 'zervisefree-qee_ro1a8jwr', {
      description: 'Issue described by your user',
      priority: 'high', // optional
      tags: ['refund', 'return'], // optional
    }).then((ticketData) => {
      // Ticket created in your Zervise Subdomain
      console.log(ticketData);
    });

    // View the tickets created by the user
    getUserTickets(data.token).then((data) => console.log(data));
  })
  .catch((err) => console.log(err));
```

- 1 step process

```js
// Authenticate user and create a ticket in a single step
authAndCreate('zervisefree-qee_ro1a8jwr', {
  name: 'test-user',
  email: 'test-user@mail.com',
  mobile: '+918555198553', // optional
  appName: 'my-app', // optional
  description: 'Issue described by your user',
  priority: 'high', // optional
  tags: ['refund', 'return'], // optional
})
  .then((data) => {
    console.log(data);

    // View the tickets created by the user
    getUserTickets(data.token).then((data) => console.log(data));
  })
  .catch((err) => console.log(err));
```

#### Reply to a ticket as the user

```js
let token;
authenticate('zervisefree-qee_ro1a8jwr', {
  name: 'test-user',
  email: 'test-user@mail.com',
  mobile: '+918555198553', // optional
  appName: 'my-app', // optional
})
  .then((data) => {
    console.log(data);

    // save the token
    token = data.token;

    // Get the tickets created by the user
    getUserTickets(data.token)
      .then((tickets) => {
        console.log(tickets);
        /* tickets array
            [
              {
                "_id": "612cd35301992244bb806ae6",
                "ticketDescription": "tciekt description text.",
                "ticketHeading": "ticket heading text.",
                "clientId": "6318f23840609d218f30019d",
                "createdPersonId": "61609dfc01992114bb806adf",
                "dateCreated": "2021-08-18T09:30:59.906Z",
                ...
              },
              {...},
              ...
            ]
            */

        // Replying to the 1st ticket
        // You can reply to any of the tickets
        // using its "_id".
        replyAsUser(token, tickets[0]._id, {
          message: 'Reply message text',
        })
          .then((ticket) => {
            // Reply successfull
            // Ticket object is returned
            console.log(ticket);
          })
          .catch((err) => console.log(err));
      })
      .catch((err) => console.log(err));
  })
  .catch((err) => console.log(err));
```

#### Get the FAQ articles of your company for users

```js
getUserFaqs('zervisefree-qee_ro1a8jwr').then((articles) => {
  // FAQ articles
  console.log(articles);
});
```

## Zervise API

#### authenticate(subdomain, {name, email[, mobile, appName]})

For authenticating user with your subdomain.

```js
const data = await authenticate('zervisefree-qee_ro1a8jwr', {
  name: 'test-user',
  email: 'test-user@mail.com',
  mobile: '+918555198553', // optional
  appName: 'my-app', // optional
});

/*
data = {
        person: <object with user details>,
        token: <authentication token for the user>,
        subdomain: <your subdomain URL>
      }
*/
```

#### createTicket(token, subdomain, {description[, priority, tags]})

For creating a ticket.

```js
const ticket = await createTicket(
  data.token, // token received from authenticate() method
  'zervisefree-qee_ro1a8jwr',
  {
    description: 'Issue described by your user',
    priority: 'high', // optional
    tags: ['refund', 'return'], // optional
  }
);

/*
ticket = <ticket object>
*/
```

#### authAndCreate(subdomain, {name, email[, mobile, appName, description, priority, tags]})

For authenticating user with your subdomain and create a ticket in a single step.

```js
const data = await authAndCreate('zervisefree-qee_ro1a8jwr', {
  name: 'test-user',
  email: 'test-user@mail.com',
  mobile: '+918555198553', // optional
  appName: 'my-app', // optional
  description: 'Issue described by your user',
  priority: 'high', // optional
  tags: ['refund', 'return'], // optional
});

/*
data = {
        person: <object with user details>,
        token: <authentication token for the user>,
        ticket: <ticket object>
        subdomain: <your subdomain URL>
      }
*/
```

#### getUserTickets(token)

For getting all the tickets of a specific user with authentication token.

```js
const tickets = await getUserTickets(data.token);
// "data.token" is the token received from authenticate() or authAndCreate() method.

/*
// array of tickets
tickets = [
  <ticket object>,
  <ticket object>,
  <ticket object>,
  ...
]
*/
```

#### replyAsUser(token, ticketId, {message})

For replying to a ticket as an user with the authentication token.

```js
// ticketId is the "_id" field inside a ticket object.
const ticket = await replyAsUser(data.token, ticket._id, {
  message: 'Reply message text',
});
// "data.token" is the token received from authenticate() or authAndCreate() method.

/*
ticket = <ticket object>
*/
```

#### getUserFaqs(subdomain)

For getting the FAQs of your company.

```js
const faqs = await getUserFaqs('zervisefree-qee_ro1a8jwr');

/*
// array of FAQ articles
faqs = [
  <FAQ object>,
  <FAQ object>,
  <FAQ object>,
  ...
]
*/
```

## Resources

- [Video Guides](https://zervise.com/resources/videos)
- [Beginners guide for Zervise admin](https://zervise.com/resources/beginners_guide/zervisesignup)
  - [Zervise Signup](https://zervise.com/resources/beginners_guide/zervisesignup)
  - [Tickets](https://zervise.com/resources/beginners_guide/tickets)
  - [Agents](https://zervise.com/resources/beginners_guide/agents)
  - [Users](https://zervise.com/resources/beginners_guide/users)
  - [User Signup and Ticket Creation](https://zervise.com/resources/beginners_guide/user_signup_and_ticket_creation)
  - [Agent Signup Process](https://zervise.com/resources/beginners_guide/agent_signup)
  - [Ticket Status](https://zervise.com/resources/beginners_guide/ticket_status)
  - [Teams](https://zervise.com/resources/beginners_guide/teams)
  - [Services](https://zervise.com/resources/beginners_guide/services)
  - [Priority List](https://zervise.com/resources/beginners_guide/priority)

## Issues

If you encounter any issue while using the package please report it here 👉 [Zervise API > Issues](https://gitlab.com/npm-zervise/npmjs-zervise/-/issues)

## License

[MIT License](https://gitlab.com/npm-zervise/npmjs-zervise/-/blob/master/LICENSE)
